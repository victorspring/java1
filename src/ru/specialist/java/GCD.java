package ru.specialist.java;

public class GCD {

    public static void main(String[] args) {
        int result = gcd(106, 16);
//        System.out.println(result);
    }

    public static int gcd(int a, int b) {
        while (b != 0) {
            int r = a % b;
            a = b;
            b = r;
        }
        return a;
    }
}

package ru.specialist.java.oop.builder;

import ru.specialist.java.oop.inheritance.Circle;
import ru.specialist.java.oop.inheritance.Rectangle;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {


    public static void main(String[] args) {

        House house = new House.Builder()
                .setDoors(1)
                .setWalls(4)
                .setWindows(2)
                .build();

        System.out.println(house);
    }

}

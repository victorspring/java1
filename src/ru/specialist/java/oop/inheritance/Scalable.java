package ru.specialist.java.oop.inheritance;

import java.io.Closeable;
import java.io.Serializable;

public interface Scalable {

    int DEFAULT_FACTOR = 2;

    void scale(double factor);

    default void scale(){
        scale(DEFAULT_FACTOR);
    }
    //16:07
}

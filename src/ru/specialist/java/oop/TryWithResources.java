package ru.specialist.java.oop;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class TryWithResources {

    public static void main(String[] args) {

        try(BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in))){


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
